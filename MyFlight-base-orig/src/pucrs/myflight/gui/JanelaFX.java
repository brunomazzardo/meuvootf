package pucrs.myflight.gui;

import java.awt.Color;
//import java.awt.TextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import javax.swing.JComboBox;
//import javax.swing.JTextArea;
//import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.viewer.GeoPosition;

import javafx.application.Application;
import javafx.embed.swing.SwingNode;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.scene.control.*;
import javafx.scene.control.ComboBox;
import pucrs.myflight.modelo.Aeronave;
import pucrs.myflight.modelo.Aeroporto;
import pucrs.myflight.modelo.CiaAerea;
import pucrs.myflight.modelo.Geo;
import pucrs.myflight.modelo.GerenciadorAeronaves;
import pucrs.myflight.modelo.GerenciadorAeroportos;
import pucrs.myflight.modelo.GerenciadorCias;
import pucrs.myflight.modelo.GerenciadorPaises;
import pucrs.myflight.modelo.GerenciadorRotas;
import pucrs.myflight.modelo.Rota;

public class JanelaFX extends Application {

	final SwingNode mapkit = new SwingNode();

	private GerenciadorCias gerCias = new GerenciadorCias();

	private GerenciadorAeroportos gerAero = new GerenciadorAeroportos();
	private GerenciadorRotas gerRotas = new GerenciadorRotas();
	private GerenciadorAeronaves gerAeronaves = new GerenciadorAeronaves();
	private GerenciadorPaises gerPais = new GerenciadorPaises();
	private GerenciadorMapa gerenciador;
	private ArrayList<Rota> consulta5 = new ArrayList<>();
	private EventosMouse mouse;
	private int consultaEscolhida;
	private String dRota = "1";
	private CiaAerea CiaEscolhida;

	private TextField distRota = new TextField();

	@Override
	public void start(Stage primaryStage) throws Exception {

		gerAero.CarregaDados();
		gerCias.carregaDados();

		gerAeronaves.carregaDados();
		gerAeronaves.adicionar(new Aeronave("000", "Rota sem Aeronave", 0));
		gerPais.carregaDados();
		gerRotas.carregaDados(gerAero, gerAeronaves, gerCias);

		GeoPosition poa = new GeoPosition(-30.05, -51.18);
		gerenciador = new GerenciadorMapa(poa, GerenciadorMapa.FonteImagens.VirtualEarth);
		mouse = new EventosMouse();
		gerenciador.getMapKit().getMainMap().addMouseListener(mouse);
		gerenciador.getMapKit().getMainMap().addMouseMotionListener(mouse);

		createSwingContent(mapkit);

		BorderPane pane = new BorderPane();
		GridPane leftPane = new GridPane();
		ComboBox<CiaAerea> BoxCia = new ComboBox<>();
		BoxCia.getItems().addAll(gerCias.listarTodas());

		leftPane.setAlignment(Pos.CENTER);
		leftPane.setHgap(10);
		leftPane.setVgap(10);

		leftPane.setPadding(new Insets(10, 10, 10, 10));
		Button btnCiaAereas = new Button("Escolher Cia");
		Button btnConsulta2 = new Button("Rotas ");
		leftPane.add(btnConsulta2, 0, 1);
		Button btnConsulta1 = new Button("Aeroportos");
		leftPane.add(btnConsulta1, 0, 0);

		Button btnLimparTela = new Button("Limpar Tela");

		BoxCia.setMaxSize(130, 130);
		distRota.setMaxSize(130, 130);
		btnCiaAereas.setMaxSize(130, 130);
		btnConsulta1.setMaxSize(130, 130);
		btnConsulta2.setMaxSize(130, 130);
		btnLimparTela.setMaxSize(130, 130);
		BoxCia.setPromptText("Cias Disponiveis");

		leftPane.add(distRota, 0, 2);
		leftPane.add(BoxCia, 0, 3);
		leftPane.add(btnCiaAereas, 0, 4);
		Button btnRotaRota = new Button("Rotas consecutivas");
		btnRotaRota.setMaxSize(130, 130);
		leftPane.add(btnRotaRota, 0, 5);

		leftPane.add(btnLimparTela, 0, 9);

		btnConsulta2.setOnAction(e -> {
			consultaEscolhida = 2;
			dRota = (distRota.getText());

		});
		btnConsulta1.setOnAction(e -> {
			consultaEscolhida = 1;
		});
		btnLimparTela.setOnAction(e -> {
			gerenciador.clear();
		});
		btnRotaRota.setOnAction(e -> {
			consultaEscolhida = 3;
		});
		btnCiaAereas.setOnAction(e -> {
			CiaEscolhida = gerCias.buscarCodigo(BoxCia.getValue().getCodigo());
			consultaEscolhida = 4;
		});

		pane.setCenter(mapkit);
		pane.setLeft(leftPane);

		Scene scene = new Scene(pane, 500, 500);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Mapas com JavaFX");
		primaryStage.show();

	}

	public Aeroporto aeroPortoNoClique(MouseEvent e) {

		JXMapViewer mapa = gerenciador.getMapKit().getMainMap();
		GeoPosition loc = mapa.convertPointToGeoPosition(e.getPoint());

		Aeroporto AeroportoMaisPerto = new Aeroporto("000", "s", new Geo(12, 12), "oi");

		for (Aeroporto c : gerAero.listarTodos()) {

			if (Math.max(loc.getLatitude(), c.getLocal().getLatitude())
					- Math.min(loc.getLatitude(), c.getLocal().getLatitude()) < 0.1
					&& Math.max(loc.getLongitude(), c.getLocal().getLongitude())
							- Math.min(loc.getLongitude(), c.getLocal().getLongitude()) < 0.1) {

				AeroportoMaisPerto = c;

			}
		}

		return AeroportoMaisPerto;

	}

	private void createSwingContent(final SwingNode swingNode) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				swingNode.setContent(gerenciador.getMapKit());
			}
		});
	}

	private class EventosMouse extends MouseAdapter {

		@Override
		public void mousePressed(MouseEvent e) {

			switch (consultaEscolhida) {

			case 1:

				desenhaAeroportosNoPais(e);
				consultaEscolhida = 0;

				break;

			case 2:

				desenhaRotas(aeroPortoNoClique(e), 1000);
				consultaEscolhida = 0;
				break;

			case 3:
				consulta3(aeroPortoNoClique(e));
				consultaEscolhida = 0;
				break;

			case 4:
				gerenciador.clear();
				consulta4();
				break;

			default:
				break;

			}

		}
	}

	public ArrayList<Rota> getRotasOrigem(Aeroporto aero, double dist) {
		ArrayList<Rota> rotas = new ArrayList<>();

		for (Rota r : gerRotas.listarTodas()) {
			if (r.getOrigem().equals(aero) && (r.getOrigem().getLocal().distancia(r.getDestino().getLocal())) < dist)
				rotas.add(r);
		}

		return rotas;
	}

	public void desenhaRotas(Aeroporto aero, double dist) {
		List<MyWaypoint> lstPoints = new ArrayList<>();
		if (dRota.isEmpty())
			dRota = "1";
		try {
			dist = Double.parseDouble(dRota);
			ArrayList<Rota> rotas = getRotasOrigem(aero, dist);
			gravaNaTela(rotas);
		} catch (Exception e) {
			distRota.setText("Somente Numeros");

		}

	}

	public void desenhaAeroporto(Aeroporto a) {
		List<MyWaypoint> lstPoints = new ArrayList<>();
		Geo g = a.getLocal();
		lstPoints.add(new MyWaypoint(Color.blue, a.getNome(), g));

		gerenciador.setPontos(lstPoints);

	}

	public ArrayList<Aeroporto> doPais(Aeroporto a) {

		ArrayList<Aeroporto> doPais = new ArrayList<>();

		for (Aeroporto c : gerAero.listarTodos()) {
			if (c.getIdentificador().equals(a.getIdentificador())) {

				doPais.add(c);
			}

		}

		return doPais;
	}

	public void desenhaAeroportosNoPais(MouseEvent e) {
		List<MyWaypoint> lstPoints = new ArrayList<>();

		for (Aeroporto c : doPais(aeroPortoNoClique(e))) {
			Geo l = c.getLocal();

			lstPoints.add(new MyWaypoint(Color.blue, c.getNome(), l));

		}
		gerenciador.setPontos(lstPoints);

	}

	public void gravaNaTela(ArrayList<Rota> rotas) {

		List<MyWaypoint> lstPoints = new ArrayList<>();

		for (Rota r : rotas) {

			Aeroporto origem = r.getOrigem();
			Aeroporto destino = r.getDestino();

			Geo locOrigem = origem.getLocal();
			Geo locDestino = destino.getLocal();
			String Label = "Aeroporto: " + r.getDestino().getNome() + ", Distancia: "
					+ String.format("%.2f", r.getOrigem().getLocal().distancia(r.getDestino().getLocal()))
					+ "KM, percorrido pela Aeronave " + r.getAeronave().getCodigo() + "\n";

			lstPoints.add(new MyWaypoint(Color.BLACK, r.getOrigem().getNome(), locOrigem));
			lstPoints.add(new MyWaypoint(Color.BLACK, Label, locDestino));

			Tracado tr = new Tracado();
			tr.addPonto(locOrigem);
			tr.addPonto(locDestino);
			tr.setCor(Color.GREEN);
			gerenciador.setPontos(lstPoints);
			gerenciador.addTracado(tr);
			gerenciador.getMapKit().repaint();
		}
		gerenciador.setPontos(lstPoints);
		gerenciador.getMapKit().repaint();
	}

	public void gravaNaTela(Rota r) {
		List<MyWaypoint> lstPoints = new ArrayList<>();
		Aeroporto origem = r.getOrigem();
		Aeroporto destino = r.getDestino();

		Geo locOrigem = origem.getLocal();
		Geo locDestino = destino.getLocal();
		String Label = "Aeroporto: " + r.getDestino().getNome() + ", Distancia: "
				+ String.format("%.2f", r.getOrigem().getLocal().distancia(r.getDestino().getLocal()))
				+ "KM, percorrido pela Aeronave " + r.getAeronave().getCodigo() + "\n";

		lstPoints.add(new MyWaypoint(Color.BLACK, r.getOrigem().getNome(), locOrigem));
		lstPoints.add(new MyWaypoint(Color.BLACK, Label, locDestino));

		Tracado tr = new Tracado();
		tr.addPonto(locOrigem);
		tr.addPonto(locDestino);
		tr.setCor(Color.GREEN);
		gerenciador.setPontos(lstPoints);
		gerenciador.addTracado(tr);
		gerenciador.getMapKit().repaint();

		gerenciador.setPontos(lstPoints);
		gerenciador.getMapKit().repaint();

	}

	public boolean jaPassou(ArrayList<Aeroporto> listaJaForam, Rota daVez) {
		for (int j = 0; j < listaJaForam.size(); j++) {
			if (daVez.getDestino().equals(listaJaForam.get(j))) {
				return false;
			}
		}

		return true;
	}

	public void consulta4() {
		ArrayList<Rota> rotaCia = new ArrayList<>();

		for (Rota r : gerRotas.listarTodas()) {
			if (r.getCia().getCodigo().equals(CiaEscolhida.getCodigo())) {
				rotaCia.add(r);
			}
		}
		gravaNaTela(rotaCia);

	}

	public void consulta5(Aeroporto inicio, Aeroporto fim) {

		for (Rota r : gerRotas.listarTodas()) {
			if (r.getDestino().equals(fim) && r.getOrigem().equals(inicio)) {

			} else {
				consulta5.add(r);
				consulta5(r.getOrigem(), fim);
			}
		}

	}

	public void consulta3(Aeroporto aeroporto) {

		ArrayList<Aeroporto> aeroportosRepetidas = new ArrayList<>();
		aeroportosRepetidas.add(aeroporto);
		printaTodasRota(aeroporto, 0, aeroportosRepetidas);

	}

	public void printaTodasRota(Aeroporto aeroporto, int count, ArrayList<Aeroporto> repetidos) {

		if (count > 2)
			return;

		ArrayList<Aeroporto> aeroportosRepetidas = new ArrayList<>(repetidos);

		for (Rota r : gerRotas.listarTodas()) {
			if (r.getOrigem().getCodigo().equals(aeroporto.getCodigo()) && jaPassou(aeroportosRepetidas, r)) {
				repetidos.add(r.getDestino());

				GeoPosition pos = gerenciador.getPosicao();

				List<MyWaypoint> lstPoints = new ArrayList<>();

				Aeroporto origem = r.getOrigem();
				Aeroporto destino = r.getDestino();

				Geo locOrigem = origem.getLocal();
				Geo locDestino = destino.getLocal();
				String Label = "Aeroporto: " + r.getDestino().getNome() + ", Distancia: "
						+ String.format("%.2f", r.getOrigem().getLocal().distancia(r.getDestino().getLocal()))
						+ "KM, percorrido pela Aeronave " + r.getAeronave().getCodigo() + "\n";

				lstPoints.add(new MyWaypoint(Color.BLACK, r.getOrigem().getNome(), locOrigem));
				lstPoints.add(new MyWaypoint(Color.BLACK, Label, locDestino));
				gerenciador.setPontos(lstPoints);

				Tracado tr = new Tracado();

				tr.addPonto(locOrigem);
				tr.addPonto(locDestino);
				tr.setCor(Color.RED);

				gerenciador.addTracado(tr);
				gerenciador.getMapKit().repaint();

				count++;
				printaTodasRota(r.getDestino(), count, aeroportosRepetidas);
				count--;
			}

		}

	}

	public static void main(String[] args) {

		launch(args);
	}

}
