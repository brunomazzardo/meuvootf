package pucrs.myflight.modelo;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import pucrs.myflight.gui.JanelaConsulta;

public class App {

    public static void main(String[] args) throws IOException {




        GerenciadorCias gerCias = new GerenciadorCias();
        gerCias.carregaDados();
        GerenciadorAeroportos gerAero = new GerenciadorAeroportos();
        GerenciadorRotas gerRotas = new GerenciadorRotas();
        GerenciadorAeronaves gerAeronaves=new GerenciadorAeronaves();
        GerenciadorPaises gerPais=new GerenciadorPaises();
        gerAero.CarregaDados();
        gerAeronaves.carregaDados();
        gerAeronaves.adicionar(new Aeronave("000","Rota sem Aeronave",0));
        gerPais.carregaDados();
        gerRotas.carregaDados(gerAero, gerAeronaves,gerCias);
        gerAero.adicionar( new Aeroporto("POA", "Salgado Filho Intl Apt", new Geo(-29.9939, -51.1711), "BR"));

        
      for(Aeroporto r:gerAero.listarTodos()){
    	  System.out.println(r.toString());
      }
        
        try {
            gerCias.gravaJSON();
        } catch (IOException e) {
            System.out.println("Impossível grava airlines.json!");
            System.out.println("Msg: "+e);
            System.exit(1);
        }

        /*
        try {
            gerCias.carregaSerial();
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Impossível ler airlines.ser!");
            System.out.println("Msg: "+e);
            System.exit(1);
        }

        try {
            gerCias.gravaSerial();
            System.out.println("Gravei airlines.ser!");
        } catch (IOException e) {
            System.out.println("Impossível gravar airlines.ser!");
            System.out.println("Msg: "+e);
            System.exit(1);
        }*/



        // Teste GUI: abre janela
        //
        JanelaConsulta janela = new JanelaConsulta();
        janela.setGerAeroportos(gerAero);
        janela.setGerRotas(gerRotas);
        janela.setVisible(true);
        //
        
    }
}