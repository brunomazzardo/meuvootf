package pucrs.myflight.modelo;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class GerenciadorAeronaves {

	private ArrayList<Aeronave> aeronaves;
	
	public GerenciadorAeronaves() {
		aeronaves = new ArrayList<>();
	}
	
	public void adicionar(Aeronave av) {
		aeronaves.add(av);	
	}
	
	public void ordenarAeronaves() {
		Collections.sort(aeronaves);
	}
	
	public void carregaDados()  {
		Path c1 = FileSystems.getDefault().getPath("dat", "equipment.dat");

		BufferedReader leitor;

		try {
			leitor = Files.newBufferedReader(c1, Charset.defaultCharset());

			String lAtual;
			leitor.readLine();
			while ((lAtual = leitor.readLine()) != null) {
				Scanner sc = new Scanner(lAtual);
				sc.useDelimiter("[;\n]");

				String codigo = sc.next();
				String descri��o = sc.next();
				String capacidade=sc.next();
				aeronaves.add(new Aeronave(codigo,descri��o,Integer.parseInt(capacidade)));
			}
		} catch (IOException e) {

		}
	}
	
	public void ordenarDescricao() {
//		aeronaves.sort( (Aeronave a1, Aeronave a2)
//				-> a1.getDescricao().compareTo(a2.getDescricao()) );
		//aeronaves.sort(Comparator.comparing(a -> a.getDescricao()));
		aeronaves.sort(Comparator.comparing(Aeronave::getDescricao));
	}
	
	public void ordenarCodigo() {
//		aeronaves.sort( (Aeronave a1, Aeronave a2)
//				-> a1.getCodigo().compareTo(a2.getCodigo()));
//		aeronaves.sort(Comparator.comparing(a -> a.getCodigo()));
		aeronaves.sort(Comparator.comparing(Aeronave::getCodigo));
	}
	
	public ArrayList<Aeronave> listarTodas() {
		return aeronaves;
	}
	
	public Aeronave buscarCodigo(String codigo) {
		for(Aeronave av : aeronaves) {
			if(codigo.equals(av.getCodigo()))
				return av;					
		}
		return new Aeronave("000","N�o Encontrada",00); // não achamos!
	}
}
