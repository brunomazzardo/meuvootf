package pucrs.myflight.modelo;

public class Aeroporto {
	private String codigo;
	private String nome;
	private Geo loc;
	private String identificador;
	
	public Aeroporto(String codigo, String nome, Geo loc,String identificador) {
		this.codigo = codigo;
		this.nome = nome;
		this.loc = loc;
		this.identificador=identificador;
				
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public String getNome() {
		return nome;
	}
	
	public Geo getLocal() {
		return loc;
	}

	@Override
	public String toString() {
		return "Aeroporto [codigo=" + codigo + ", nome=" + nome + ", loc=" + loc + ", identificador=" + identificador
				+ "]";
	}
}
