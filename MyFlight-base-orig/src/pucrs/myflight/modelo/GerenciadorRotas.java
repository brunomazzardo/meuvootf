package pucrs.myflight.modelo;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class GerenciadorRotas {

	private ArrayList<Rota> rotas;

	public GerenciadorRotas() {
		rotas = new ArrayList<>();
	}

	public void adicionar(Rota r) {
		rotas.add(r);
	}

	public void carregaDados(GerenciadorAeroportos ga, GerenciadorAeronaves gv, GerenciadorCias gc) {
		Path c1 = FileSystems.getDefault().getPath("dat", "routes.dat");
		Rota nova;

		BufferedReader leitor;

		try {
			leitor = Files.newBufferedReader(c1, Charset.defaultCharset());

			String lAtual;
			leitor.readLine();
			leitor.readLine();
			while ((lAtual = leitor.readLine()) != null) {

				String codigo, CodOrigem, CodDestino, CodAeronave;
				Scanner sc = new Scanner(lAtual);
				sc.useDelimiter("[;\n]");
				CodAeronave = null;
				int cont = 0;

				codigo = sc.next();

				CodOrigem = sc.next();

				CodDestino = sc.next();
				sc.next();
				sc.next();

				if (sc.hasNext()) {

					String cod = sc.next().substring(0, 3);
					Aeronave aeronave = gv.buscarCodigo(cod);

					nova = new Rota(gc.buscarCodigo(codigo), ga.buscarCodigo(CodOrigem), ga.buscarCodigo(CodDestino),
							aeronave);
				} else {

					nova = new Rota(gc.buscarCodigo(codigo), ga.buscarCodigo(CodOrigem), ga.buscarCodigo(CodDestino),
							gv.buscarCodigo("000"));
				}
				rotas.add(nova);

				sc.close();
			}

		} catch (IOException e) {

		}
	}

	public void ordenarCia() {
		rotas.sort((Rota r1, Rota r2) -> r1.getCia().getNome().compareTo(r2.getCia().getNome()));
	}

	public void ordenarOrigem() {
		rotas.sort((Rota r1, Rota r2) -> r1.getOrigem().getNome().compareTo(r2.getOrigem().getNome()));
	}

	public void ordenarOrigemCia() {
		rotas.sort((Rota r1, Rota r2) -> {
			int res = r1.getOrigem().getNome().compareTo(r2.getOrigem().getNome());
			if (res != 0)
				return res;
			// Desempata pelo nome da cia.
			return r1.getCia().getNome().compareTo(r2.getCia().getNome());
		});
		// ou:
		rotas.sort(Comparator.comparing((Rota r) -> r.getOrigem().getNome()).thenComparing(r -> r.getCia().getNome()));
	}

	public ArrayList<Rota> listarTodas() {
		return new ArrayList<Rota>(rotas);
	}

	public ArrayList<Rota> buscarOrigem(Aeroporto origem) {
		ArrayList<Rota> lista = new ArrayList<>();
		for (Rota r : rotas) {
			if (origem.getCodigo().equals(r.getOrigem().getCodigo()))
				lista.add(r);
		}
		return lista;
	}
}
