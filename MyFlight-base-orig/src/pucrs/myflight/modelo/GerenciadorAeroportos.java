package pucrs.myflight.modelo;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class GerenciadorAeroportos {

	private ArrayList<Aeroporto> aeroportos;

	public GerenciadorAeroportos() {
		aeroportos = new ArrayList<>();
	}

	public void adicionar(Aeroporto a) {
		aeroportos.add(a);
	}

	public void CarregaDados() {
		Path c1 = Paths.get("dat\\airports.dat");
		try {
			BufferedReader leitor = Files.newBufferedReader(c1, Charset.defaultCharset());
			
			String lAtual;
			leitor.readLine();
          
           while ((lAtual = leitor.readLine()) != null){
        	  
				Scanner sc = new Scanner(lAtual);
				sc.useDelimiter("[;\n]");

				String iataCode = sc.next();
				String latitude = sc.next();
			String longitude = sc.next();
				String airportName = sc.next();
				String identificador = sc.next();

				aeroportos.add(new Aeroporto(iataCode, airportName,
						new Geo(Double.parseDouble(latitude), Double.parseDouble(longitude)), identificador));

				sc.close();
			}

		} catch (IOException e) {

		}

	}
	public ArrayList<Aeroporto> listarTodos() {
		return aeroportos;
	}

	public Aeroporto buscarCodigo(String codigo) {
		for (Aeroporto a : aeroportos) {
			if (codigo.equals(a.getCodigo()))
				return a;
		}
		return null; // não achamos!
	}
}
